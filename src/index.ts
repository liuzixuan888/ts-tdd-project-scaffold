export const add = (a: number, b: number): number => a + b;

// 目标输出第二天的价值
const queryInventoryValue = (goodsType: string, currentSellIn: number, currentQuality: number) => {
  // currentSellIn：是商品距离过期的天数
  // currentQuality: 代表商品的当前价值

  //1.商品的价值永远不会小于 0，也永远不会超过 50
  //2.商品每过一天价值会下滑 1 点 ，一旦过了保质期，价值就以双倍的速度下滑
  if (currentQuality > 50 || currentQuality < 0) {
    throw new RangeError("商品的价值永远不会小于 0，也永远不会超过 50!");
  }
  var newQuality = currentQuality
  var pic = 0
  if (goodsType === '常规商品') {
    if (currentSellIn > 1) {
      pic = 1
    } else {
      pic = 2
    }
    newQuality = currentQuality - pic
  } else if (goodsType === '后台门票') {
    if (currentSellIn == 0) {
      newQuality = 0
    } else if (currentSellIn <= 5) {
      pic = 3
    } else if (currentSellIn <= 10) {
      pic = 2
    } else if (currentSellIn <= 15) {
      pic = 1
    }
    newQuality = currentQuality - pic
  } else if (goodsType === '萨弗拉斯') {
    pic = 5
    if (currentSellIn >= 0) {
      newQuality = currentQuality
    } else if (currentSellIn < 0) {
      newQuality = currentQuality + pic
    }
  } else if (goodsType === '陈年干酪') {
    if (currentSellIn <= 0) {
      pic = 2
      newQuality = currentQuality + pic
    } else {
      pic = 1
      newQuality = currentQuality + pic
    }
  } else {
    throw new RangeError("您输入的的商品类型有误");
  }
  if (newQuality < 0) {
    newQuality = 0
  } else if (newQuality > 50) {
    newQuality = 50
  }
  return newQuality
}